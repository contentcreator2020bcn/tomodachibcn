# client

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### URLs
dev - http://dev.restaurantecasamuyo.com.s3-website.eu-west-3.amazonaws.com/
prod - http://www.restaurantecasamuyo.com/
